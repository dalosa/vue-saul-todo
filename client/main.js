import { Meteor } from 'meteor/meteor';
import Vue from 'vue';
import VueTracker from 'vue-meteor-tracker';
Vue.use(VueTracker);
import App from '/imports/ui/App.vue';
import { Todos } from "../imports/api/todos.js";
import { Users } from "../imports/api/users.js";
import { GDomains } from "../imports/api/gdomains";
import VueDraggable from 'vue-draggable'
 
Vue.use(VueDraggable)
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)
Meteor.startup(() => {
  new Vue({
    render: h => h(App),
  }).$mount('app');
});

